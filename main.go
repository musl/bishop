package main

import (
	"fmt"
	"log"
	"os"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/musl/bishop/lib/bot"
)

var (
	// AppName is the single word used to identify this program in logs,
	// etc.
	AppName = "bishop"

	// Version is the semantic version string of this app.
	Version = ""

	// Revision is the DVCS change set identifier that this code was
	// built on.
	Revision = ""
)

// Config is all of the knobs that we need to expose through the
// command-line.
type Config struct {
	BotToken      string `required:"true" split_words:"true"`
	SigningSecret string `required:"true" split_words:"true"`

	BindAddr string `default:"0.0.0.0:8000" split_words:"true"`
	Debug    bool   `default:"false"`
}

func main() {
	logger := log.New(
		os.Stderr,
		AppName+` `,
		log.Ldate|log.Ltime|log.Lshortfile,
	)

	config := Config{}
	err := envconfig.Process(AppName, &config)
	if err != nil {
		logger.Fatal(err)
	}

	if config.BotToken == "" {
		logger.Fatal(fmt.Errorf("empty slack token"))
	}

	logger.Println(AppName, Version, Revision)

	bot, err := bot.New(&bot.Config{
		Logger:        logger,
		Token:         config.BotToken,
		SigningSecret: config.SigningSecret,
		Debug:         config.Debug,
	})

	if err != nil {
		log.Fatal(err)
	}

	logger.Fatal(bot.Start(config.BindAddr))
}
