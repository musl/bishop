module gitlab.com/musl/bishop

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/prometheus/client_golang v1.8.0
	github.com/slack-go/slack v0.7.2
)
