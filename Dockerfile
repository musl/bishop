FROM ubuntu:latest as builder
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt clean &&\
	apt update &&\
   	apt install -y make git golang
ADD . /bishop
WORKDIR /bishop
RUN make

FROM ubuntu:latest
LABEL maintainer="Mike Hix <m@hix.io>"
LABEL description="A slack bot."
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt clean &&\
	apt update &&\
   	apt install -y ca-certificates
COPY --from=builder /bishop/bishop /bishop
EXPOSE 8000
ENTRYPOINT ["/bishop"]
