BIN := $(shell basename $(CURDIR))
VERSION := $(shell git describe --abbrev=0 --tags 2>/dev/null || echo 'v0.0.0')
REVISION := $(shell git rev-list -1 HEAD 2>/dev/null || echo 'NONE')

.PHONY: all clean image run

ifeq ("$(wildcard .env)","")
$(error Please populate `.env`. See `docker-compose.yml` for what variables you need)
endif

include .env
export

all: clean test $(BIN)

clean:
	rm -f $(BIN)
	go clean .

test:
	go test -v .

$(BIN):
	go build -o $@ -ldflags '-X main.Version=$(VERSION) -X main.Revision=$(REVISION)' .

image:
	docker build -t bishop:latest .

up: image
	docker-compose up

down:
	docker-compose down -v

run: clean $(BIN)
	./bishop

proxy:
	npx localtunnel -s mhix-bot-bishop --port 8000

