package bot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/slack-go/slack/slackevents"
)

func commandInfo(b *Bot, e *slackevents.MessageEvent) error {
	info, err := b.MarshalSafe()
	if err != nil {
		return err
	}
	return b.sendMessage(e.Channel, "```"+info+"```")
}

func commandRoll(b *Bot, e *slackevents.MessageEvent) error {
	n := 1
	m := 20
	_, err := fmt.Sscanf(e.Text, "!roll %dd%d", &n, &m)
	if err != nil || n < 1 || m < 1 {
		return b.sendMessage(
			e.Channel,
			"Usage: `!roll XdY` where `X` is the whole natural of dice and `Y` is the natural number of sides they have.",
		)
	}

	results := []int{}
	for i := 0; i < n; i++ {
		results = append(results, 1+rand.Intn(m))
	}

	sum := 0
	min := m
	max := 0
	for _, v := range results {
		sum += v
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}

	if n < 2 {
		return b.sendMessage(
			e.Channel,
			fmt.Sprintf(
				"Your roll: %s",
				strings.Trim(strings.Replace(fmt.Sprint(results), " ", ", ", -1), "[]"),
			),
		)
	}

	return b.sendMessage(
		e.Channel,
		fmt.Sprintf(
			"Your roll: %s\nSum: %d Min: %d Max: %d",
			strings.Trim(strings.Replace(fmt.Sprint(results), " ", ", ", -1), "[]"),
			sum,
			min,
			max,
		),
	)
}

func commandTime(b *Bot, e *slackevents.MessageEvent) error {
	return b.sendMessage(
		e.Channel,
		fmt.Sprintf(
			"The time is: %s\nI have been running for %s.",
			time.Now(),
			b.Uptime(),
		),
	)
}

func commandWeather(b *Bot, e *slackevents.MessageEvent) error {
	lat := 45.5
	lon := -122.66

	_, err := fmt.Sscanf(e.Text, "!weather %f,%f", &lat, &lon)
	if err != nil {
		b.sendMessage(
			e.Channel,
			fmt.Sprintf("No location provided. Using: %f,%f\n _Hint: `!weather <latitude>,<longitude>`_", lat, lon),
		)
	}

	client := &http.Client{}

	pointsURL := fmt.Sprintf("https://api.weather.gov/points/%f,%f", lat, lon)
	req, err := http.NewRequest("GET", pointsURL, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Accept", "application/geo+json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	point := &struct {
		Properties struct {
			Forecast string `json:"forecast"`
		} `json:"properties"`
	}{}
	err = json.Unmarshal(body, &point)
	if err != nil {
		return err
	}

	req, err = http.NewRequest("GET", point.Properties.Forecast, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Accept", "application/geo+json")

	res, err = client.Do(req)
	if err != nil {
		return err
	}

	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	forecast := &struct {
		Properties struct {
			Periods []struct {
				Number           int    `json:"number"`
				Name             string `json:"name"`
				DetailedForecast string `json:"detailedForecast"`
			} `json:"periods"`
		} `json:"properties"`
	}{}
	err = json.Unmarshal(body, &forecast)
	if err != nil {
		return err
	}

	forecasts := forecast.Properties.Periods
	sort.Slice(forecasts, func(i, j int) bool {
		return forecasts[i].Number < forecasts[j].Number
	})

	sb := strings.Builder{}
	sb.WriteString("Here's your forecast:\n\n")
	for _, f := range forecasts[:2] {
		fmt.Fprintf(&sb, "*%s:*\n%s\n\n", f.Name, f.DetailedForecast)
	}

	return b.sendMessage(e.Channel, sb.String())
}
