package bot

import (
	"context"
	"encoding/json"
	"log"
	"regexp"
	"time"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"

	"github.com/prometheus/client_golang/prometheus"
)

// Config is all of the configurable knobs for a bot.
type Config struct {
	Logger        *log.Logger
	Debug         bool
	Token         string
	SigningSecret string
}

// CommandHandlerFunc is
type CommandHandlerFunc func(b *Bot, e *slackevents.MessageEvent) error

// MetricSet stores statistics about this bot
type MetricSet struct {
	EventDuration           prometheus.Summary
	EventCount              prometheus.Gauge
	EventDispatchErrorCount prometheus.Gauge
	MentionCount            prometheus.Gauge
	MessageCount            prometheus.Gauge
	UnhandledEventCount     prometheus.Gauge
}

// NewMetricSet initializes and registers all of the metrics.
func NewMetricSet(namespace string) MetricSet {
	ms := MetricSet{
		EventDuration: prometheus.NewSummary(prometheus.SummaryOpts{
			Namespace: namespace,
			Name:      "event_duration",
			Help:      "Summary of event durations",
		}),
		EventCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "event_count",
			Help:      "Number of events received by this instance",
		}),
		EventDispatchErrorCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "event_dispatch_error_count",
			Help:      "Number of events that returned errors during dispatch",
		}),
		MentionCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "mention_count",
			Help:      "Number of mention events received by this instance",
		}),
		MessageCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "message_count",
			Help:      "Number of messages received by this instance",
		}),
		UnhandledEventCount: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "unhandled_event_count",
			Help:      "Number of events that were not handled during dispatch",
		}),
	}

	prometheus.MustRegister(ms.EventDuration)
	prometheus.MustRegister(ms.EventCount)
	prometheus.MustRegister(ms.EventDispatchErrorCount)
	prometheus.MustRegister(ms.MentionCount)
	prometheus.MustRegister(ms.MessageCount)
	prometheus.MustRegister(ms.UnhandledEventCount)

	return ms
}

// Bot is all of the knobs needed to run a bot.
type Bot struct {
	SlackClient      *slack.Client
	SigningSecret    string
	SignatureVersion string
	MaxEventSize     int64

	Logger *log.Logger
	Debug  bool

	id     string
	team   string
	teamID string
	user   string
	userID string

	started  time.Time
	commands map[string]CommandHandlerFunc
	metrics  MetricSet
}

// Uptime returns how long this bot has been running.
func (b *Bot) Uptime() time.Duration {
	return time.Now().Sub(b.started)
}

// MarshalSafe implements Stringer only returning non-secret information.
func (b *Bot) MarshalSafe() (string, error) {
	j, err := json.MarshalIndent(struct {
		ID      string
		Team    string
		TeamID  string
		User    string
		UserID  string
		Debug   bool
		Started time.Time
		Uptime  string
	}{
		ID:      b.id,
		Team:    b.team,
		TeamID:  b.teamID,
		User:    b.user,
		UserID:  b.userID,
		Started: b.started,
		Uptime:  b.Uptime().String(),
	}, "", "  ")
	if err != nil {
		return "", err
	}

	return string(j), nil
}

// ID returns the ID of the bot.
func (b *Bot) ID() string {
	return b.id
}

// Team returns the team name that the bot belongs to.
func (b *Bot) Team() string {
	return b.team
}

// TeamID returns the id of the team that the bot belongs to.
func (b *Bot) TeamID() string {
	return b.teamID
}

// User returns the name of the bot user.
func (b *Bot) User() string {
	return b.user
}

// UserID returns the id of the bot user.
func (b *Bot) UserID() string {
	return b.userID
}

var defaultCommands = map[string]CommandHandlerFunc{
	"info":    commandInfo,
	"roll":    commandRoll,
	"time":    commandTime,
	"weather": commandWeather,
}

// New makes a new Bot and returns a reference to it.
func New(config *Config) (*Bot, error) {
	client := slack.New(
		config.Token,
		slack.OptionDebug(config.Debug),
		slack.OptionLog(config.Logger),
	)

	authTestResponse, err := client.AuthTest()
	if err != nil {
		return nil, err
	}

	b := &Bot{
		SlackClient:      client,
		SigningSecret:    config.SigningSecret,
		SignatureVersion: "v0",
		MaxEventSize:     4 * 1024 * 1024, // bytes
		Logger:           config.Logger,
		Debug:            config.Debug,

		id:     authTestResponse.BotID,
		team:   authTestResponse.Team,
		teamID: authTestResponse.TeamID,
		user:   authTestResponse.User,
		userID: authTestResponse.UserID,

		commands: make(map[string]CommandHandlerFunc),
		metrics:  NewMetricSet(authTestResponse.User),
	}

	for k, v := range defaultCommands {
		b.Command(k, v)
	}

	return b, nil
}

func (b *Bot) sendMessage(channel string, message string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, _, err := b.SlackClient.PostMessageContext(
		ctx,
		channel,
		slack.MsgOptionText(message, false),
	)

	return err
}

// Command registers a new command with the given name.
func (b *Bot) Command(name string, chf CommandHandlerFunc) {
	b.commands[name] = chf
}

func (b *Bot) dispatch(event slackevents.EventsAPIEvent) (err error) {
	dispatchStart := time.Now()
	innerEvent := event.InnerEvent

	b.metrics.EventCount.Add(1)

	switch e := innerEvent.Data.(type) {
	case *slackevents.AppMentionEvent:
		b.metrics.MentionCount.Add(1)
		err = b.dispatchMention(e)
	case *slackevents.MessageEvent:
		b.metrics.MessageCount.Add(1)
		err = b.dispatchMessage(e)
	default:
		b.metrics.UnhandledEventCount.Add(1)
		if b.Debug {
			b.Logger.Println("Unhandled event: ", innerEvent.Type)
		}
	}

	if err != nil {
		b.metrics.EventDispatchErrorCount.Add(1)
	}

	b.metrics.EventDuration.Observe(float64(time.Now().Sub(dispatchStart).Milliseconds()))
	return
}

func (b *Bot) dispatchMessage(e *slackevents.MessageEvent) error {
	// Don't respond to bots.
	// TODO: Test whether the user check is necessary along side the BotID check
	if e.BotID != "" || e.User == b.user {
		return nil
	}

	pattern := regexp.MustCompile(`^!(\w+)`)
	matches := pattern.FindStringSubmatch(e.Text)
	if len(matches) < 2 {
		return nil
	}

	if cmd, ok := b.commands[matches[1]]; ok {
		return cmd(b, e)
	}

	return b.sendMessage(e.Channel, "I'm sorry, I don't know that command.")
}

func (b *Bot) dispatchMention(e *slackevents.AppMentionEvent) error {
	// Don't respond to bots.
	if e.BotID != "" {
		return nil
	}

	return nil
}
