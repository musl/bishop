package bot

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/slack-go/slack/slackevents"
)

func validateSignature(body string, b *Bot, c *gin.Context) bool {
	signature := c.GetHeader("X-Slack-Signature")

	base := strings.Join([]string{
		b.SignatureVersion,
		c.GetHeader("X-Slack-Request-Timestamp"),
		body,
	}, ":")

	mac := hmac.New(sha256.New, []byte(b.SigningSecret))
	mac.Write([]byte(base))
	expectedSum := strings.Join([]string{
		b.SignatureVersion,
		hex.EncodeToString(mac.Sum(nil)),
	}, "=")

	return signature == expectedSum
}

func handleStatusCheck(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{})
}

func handleEvents(c *gin.Context) {
	b := c.MustGet(`bot`).(*Bot)

	c.Request.Body = http.MaxBytesReader(c.Writer, c.Request.Body, b.MaxEventSize)

	buf := new(bytes.Buffer)
	buf.ReadFrom(c.Request.Body)
	body := buf.String()

	if validateSignature(body, b, c) == false {
		c.JSON(http.StatusUnauthorized, nil)
		return
	}

	event, err := slackevents.ParseEvent(
		json.RawMessage(body),
		slackevents.OptionNoVerifyToken(),
	)
	if err != nil {
		b.Logger.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	switch event.Type {
	// TODO maybe break out verification handling into middleware too.
	case slackevents.URLVerification:
		r, ok := event.Data.(slackevents.ChallengeResponse)
		if !ok {
			b.Logger.Println(err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.String(http.StatusOK, r.Challenge)

	case slackevents.AppRateLimited:
		arl, ok := event.Data.(slackevents.EventsAPIAppRateLimited)
		if !ok {
			b.Logger.Println(err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		b.Logger.Println("Rate Limited: ", arl.MinuteRateLimited)
		c.JSON(http.StatusOK, gin.H{})

	case slackevents.CallbackEvent:
		if err := b.dispatch(event); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, gin.H{})
		}

	default:
		if b.Debug {
			b.Logger.Println(event)
		}
		c.JSON(http.StatusOK, gin.H{})
	}
}

func inject(key string, value interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(key, value)
		c.Next()
	}
}

// Start starts a bot.
func (b *Bot) Start(bindAddr string) error {
	router := gin.Default()

	if b.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	router.Use(inject(`bot`, b))

	router.GET(`/status/check`, handleStatusCheck)
	router.GET(`/metrics`, gin.WrapH(promhttp.HandlerFor(
		prometheus.DefaultGatherer,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	)))
	router.POST(`/events`, handleEvents)

	b.started = time.Now()

	return router.Run(bindAddr)
}
