# bishop
A slack bot.

## Setup

1. Setup a slack app, grant it a bunch of rights I'll document later. It needs mention read, chat read, write at least.
2. Collect a bot token and a signing secret from the config UI at https://api.slack.com/apps under your app.
3. Write those secrets to a file called `.env` in the root of the repo. For example:
        
				BISHOP_BOT_TOKEN=...
				BISHOP_SIGNING_SECRET=...

4. Follow the "Running Locally" or "Running Locally With Style" step below.
5. Enable events for your app; the verification URL is the one that the `make proxy` command gives you with `/events` tacked on the end. 

## Running Locally

1. In a terminal, run: `make proxy` 
2. In another terminal, run: `make run`

## Running Locally _With Style_

Run stuff:
1. In a terminal, run: `make proxy` 
2. In another terminal, run: `make up`

Visit stuff:
- Prometheus: http://localhost:9090/
- Grafana: http://localhost:3000/

Clean up after yourself:
1. Run `make down`
2. Optionally run `docker system prune`

## Tests

1. Run `make test`
 
